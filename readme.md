# SampleSearchApp
This is a sample application that illustrates some basic search functionality.

## To Run from IDE
This project can be run by loading it into an IDE and running the "main" method
in project class (SampleSearchApp.java). Alternatively, it can be built and 
packaged into an executable jar file using Maven (instructions below).

#### Running the executable Jar file
Once compiled, simply run `java -jar ./SampleSearchApp.jar` from a command 
prompt/terminal on a machine that has the Java runtime installed. The application
will prompt for input in the same window.

## To Import to an IDE
This project is built using Maven and should be recognized by either Eclipse
or IntelliJ. It should be imported as a Java Maven project and the pom.xml file
in the top-level directory should be referenced.

## Building and Packaging with Maven

#### Using a Maven plugin from an IDE
This project is built using Maven. The easiest way to build this will be
by importing it into either Eclipse or IntelliJ and running the `clean`
and `package` goals using the provided Maven plugin.

#### Using a local Maven install from the terminal or command prompt
Alternatively, if you have Maven installed on your computer, you can simply
run `mvn clean package` from the command line in the same directory as the
pom.xml file.

After running the 'package' goal, the executable jar file will be created
in the `target/` directory.

## Running Unit Tests
This application is utilizing the JUnit4 testing suite. It is also integrated 
with Maven and is attached to the `test` and `package` goals. It can be run from the command line
by issuing the command `mvn test`, or run inside an IDE by running the `test` goal. It will
also be run whenever the `package` goal is run.

The unit tests will run with the same classpath information as the executable
jar file, but anything in the `test-resources` classpath will also be loaded when the
unit tests are being run.

Results from the unit tests will be both printed in the console as well as exported to
the `target\surefire-reports` folder.

## Assumptions
I have assumed a few things while designing this application given the set of sample data provided:  
1) There will never be a negative integer ID for any value of any field  
2) For demonstration purposes, I've modeled the timestamps as Strings to avoid deserializing the timestamp format  
3) To keep the application simple, when searching through fields with boolean values, any search term entered other than 
'true' will resolve to false  
4) Mocking input to a terminal-based input using JUnit during a Maven build is very hard. I have written tests to 
illustrate that searching for a blank description on the TicketRepository works, although I did not take the final 
step to set up something like Mockito to truly mock that input  
