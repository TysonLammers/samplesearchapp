package com.lammers.sampleSearchApp.utils;

import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;


public class FileUtilsTest extends TestCase {
    public FileUtilsTest() {
        super();
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    public void testFileNotFoundFails() {
        try {
            FileUtils.readFileContentsFromClasspath("this-file-doesn't-exist");
        } catch (NullPointerException e) {
            // return to pass
            return;
        }
        fail();
    }

    public void testFileLoadsFromTestResources() {
        String contents = null;
        try {
            contents = FileUtils.readFileContentsFromClasspath("empty-array.json");
            assertTrue("Failed to load file from test resources directory", StringUtils.isNotEmpty(contents));
        } catch (Exception e) {
            fail(e.toString());
        }
    }

}
