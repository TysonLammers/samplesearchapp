package com.lammers.sampleSearchApp.repository;

import com.lammers.sampleSearchApp.model.User;
import junit.framework.TestCase;

import java.util.List;

public class UserRepositoryTest extends TestCase {
    private UserRepository userRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        userRepository = new UserRepository();
    }

    /**
     * Also tests getSize()
     */
    public void testEmptyConstructorWorks() {
        assertTrue(userRepository.getUsers().size() > 0);
        assertTrue(userRepository.getSize() > 0);
    }

    /**
     * Also tests getSize()
     */
    public void testLoadEmptyJsonFile() {
        userRepository = new UserRepository("empty-array.json");
        assertEquals(0, userRepository.getUsers().size());
        assertEquals(0, userRepository.getSize());
    }

    /**
     * Also tests getSize()
     */
    public void testLoadSingleUser() {
        userRepository = new UserRepository("user-single.json");
        assertEquals(1, userRepository.getUsers().size());
        assertEquals(1, userRepository.getSize());
        User user = userRepository.getUsers().get(0);
        assertNotNull(user);
        assertEquals(1, user.getId());
        assertEquals("http://initech.zendesk.com/api/v2/users/1.json", user.getUrl());
        assertEquals("74341f74-9c79-49d5-9611-87ef9b6eb75f", user.getExternalId());
        assertEquals("Francisca Rasmussen", user.getName());
        assertEquals("Miss Coffey", user.getAlias());
        assertEquals("2016-04-15T05:19:46 -10:00", user.getCreatedAt());
        assertEquals(true, user.isActive());
        assertEquals(true, user.isVerified());
        assertEquals(user.isShared(), false);
    }

    /**
     * A sample of the tests that would be included in a full test suite
     */
    public void testSearchForUser() {
        userRepository = new UserRepository("user-single.json");
        List<User> matches = userRepository.findUsersByFieldAndValue(User.UserField.ACTIVE, "true");
        assertTrue(matches.size() == 1);
        matches = userRepository.findUsersByFieldAndValue(User.UserField.URL, "http://initech.zendesk.com/api/v2/users/1.json");
        assertTrue(matches.size() == 1);
    }
}
