package com.lammers.sampleSearchApp.repository;

import junit.framework.TestCase;

public class OrganizationRepositoryTest extends TestCase {
    private OrganizationRepository organizationRepository;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        organizationRepository = new OrganizationRepository();
    }

    /**
     * Also tests getSize()
     */
    public void testEmptyConstructorWorks() {
        assertTrue(organizationRepository.getOrganizations().size() > 0);
        assertTrue(organizationRepository.getSize() > 0);
    }

    /**
     * Also tests getSize()
     */
    public void testLoadEmptyJsonFile() {
        organizationRepository = new OrganizationRepository("empty-array.json");
        assertEquals(0, organizationRepository.getOrganizations().size());
        assertEquals(0, organizationRepository.getSize());
    }

    /**
     * Also tests getSize()
     */
    public void testLoadSingleOrganization() {
        organizationRepository = new OrganizationRepository("organization-single.json");
        assertEquals(1, organizationRepository.getOrganizations().size());
        assertEquals(1, organizationRepository.getSize());
    }
}
