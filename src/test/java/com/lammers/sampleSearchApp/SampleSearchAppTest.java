package com.lammers.sampleSearchApp;

import com.lammers.sampleSearchApp.model.Ticket;
import com.lammers.sampleSearchApp.repository.TicketRepository;
import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

public class SampleSearchAppTest extends TestCase {
    private SampleSearchApp searchApp;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        searchApp = new SampleSearchApp();
    }

    public void testAppRunsWithClasspathDefaultFiles() {
        assertNotNull("you broke the unit tests (hopefully on purpose)", searchApp);
        assertFalse("Failed to deserialize data from the default organizations.json file", searchApp.getOrganizationRepository().getSize() == 0);
        assertFalse("Failed to deserialize data from default tickets.json file", searchApp.getTicketRepository().getSize() == 0);
        assertFalse("Failed to deserialize data from default users.json file", searchApp.getUserRepository().getSize() == 0);
    }
    
    public void testAppRunsAndQuits() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream in = new ByteArrayInputStream("quit\n".getBytes());

        searchApp = new SampleSearchApp(in, new PrintStream(outputStream));
        searchApp.runMainLoop();

        assertTrue("App did not exit when 'quit' was entered", searchApp.isShouldExit());
    }

    /**
     * Make sure we can search for a ticket with an empty description.
     */
    public void testCanSearchForEmptyDescriptionTicket() {
        searchApp = new SampleSearchApp();
        searchApp.setTicketRepository(new TicketRepository("ticket-single-empty-description.json"));
        List<Ticket> matchingTickets = searchApp.getTicketRepository().findTickesByFieldAndValue(Ticket.TicketField.DESCRIPTION, "");
        assertFalse("Unable to match on an empty description field for a ticket", matchingTickets.isEmpty());
    }
}
