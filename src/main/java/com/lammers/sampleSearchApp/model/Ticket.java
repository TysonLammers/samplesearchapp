package com.lammers.sampleSearchApp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Ticket {
    private static final Logger LOGGER = LogManager.getLogger(Ticket.class);
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    //gson doesn't like fields that start with underscores
    @SerializedName("_id")
    private String id;
    private String url;
    private String externalId;
    private String createdAt; // this is actually a timestamp but requirements state 'match string'
    private String type;
    private String subject;
    private String description;
    private String priority;
    private String status;
    private int submitterId;
    private int assigneeId;
    private int organizationId;
    private List<String> tags;
    private boolean hasIncidents;
    private String dueAt;
    private String via;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSubmitterId() {
        return submitterId;
    }

    public void setSubmitterId(int submitterId) {
        this.submitterId = submitterId;
    }

    public int getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(int assigneeId) {
        this.assigneeId = assigneeId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isHasIncidents() {
        return hasIncidents;
    }

    public void setHasIncidents(boolean hasIncidents) {
        this.hasIncidents = hasIncidents;
    }

    public String getDueAt() {
        return dueAt;
    }

    public void setDueAt(String dueAt) {
        this.dueAt = dueAt;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    /**
     * Determines whether this Ticket is a match given the field to search
     * and value to search for
     *
     * @param userSearchField
     * @param searchValue
     * @return
     */
    public boolean isSearchFieldMatch(TicketField userSearchField, String searchValue) {
        switch (userSearchField) {
            case ID:
                return StringUtils.equals(this.getId(), searchValue);
            case URL:
                return StringUtils.equals(this.getUrl(), searchValue);
            case EXTERNAL_ID:
                return StringUtils.equals(this.getExternalId(), searchValue);
            case CREATED_AT:
                return StringUtils.equals(this.getCreatedAt(), searchValue);
            case TYPE:
                return StringUtils.equals(this.getType(), searchValue);
            case SUBJECT:
                return StringUtils.equals(this.getSubject(), searchValue);
            case DESCRIPTION:
                return StringUtils.equals(this.getDescription(), searchValue);
            case PRIORITY:
                return StringUtils.equals(this.getPriority(), searchValue);
            case STATUS:
                return StringUtils.equals(this.getStatus(), searchValue);
            case SUBMITTER_ID:
                try {
                    return this.getSubmitterId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for Submitter ID");
                }
                break;
            case ASSIGNEE_ID:
                try {
                    return this.getAssigneeId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for Assignee ID");
                }
                break;
            case ORGANIZATION_ID:
                try {
                    return this.getOrganizationId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for Organization ID");
                }
                break;
            case TAGS:
                for (String tag : this.getTags()) {
                    if (StringUtils.equals(tag, searchValue)) {
                        return true;
                    }
                }
                break;
            case HAS_INCIDENTS:
                return this.isHasIncidents() == Boolean.parseBoolean(searchValue);
            case DUE_AT:
                return StringUtils.equals(this.getDueAt(), searchValue);
            case VIA:
                return StringUtils.equals(this.getVia(), searchValue);
        }
        return false;
    }

    /**
     * Override the default toString to print this organization in Json
     *
     * @return
     */
    @Override
    public String toString() {
        return GSON.toJson(this);
    }


    /**
     * This class gives us the 'magic' we need to be able to take a string search term
     * and match it to the desired field we're trying to search for.
     */
    public enum TicketField {
        ID("_id"),
        URL("url"),
        EXTERNAL_ID("external_id"),
        CREATED_AT("created_at"),
        TYPE("type"),
        SUBJECT("subject"),
        DESCRIPTION("description"),
        PRIORITY("priority"),
        STATUS("status"),
        SUBMITTER_ID("submitter_id"),
        ASSIGNEE_ID("assignee_id"),
        ORGANIZATION_ID("organization_id"),
        TAGS("tags"),
        HAS_INCIDENTS("has_incidents"),
        DUE_AT("due_at"),
        VIA("via");

        private final String displayText;

        TicketField(String displayText) {
            this.displayText = displayText;
        }

        public static TicketField getTicketFieldFromDisplayText(String displayText) {
            for (TicketField field : TicketField.values()) {
                if (StringUtils.equals(field.getDisplayText(), displayText)) {
                    return field;
                }
            }
            return null;
        }

        public String getDisplayText() {
            return displayText;
        }

    }

}
