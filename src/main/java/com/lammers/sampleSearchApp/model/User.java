package com.lammers.sampleSearchApp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class User {
    private static final Logger LOGGER = LogManager.getLogger(User.class);
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    @SerializedName("_id")
    private int id;
    private String url;
    private String externalId;
    private String name;
    private String alias;
    private String createdAt;
    private boolean active;
    private boolean verified;
    private boolean shared;
    private String locale;
    private String timezone;
    private String lastLoginAt; // this is actually a timestamp but requirements state 'match string'
    private String email;
    private String phone;
    private String signature;
    private int organizationId;
    private List<String> tags;
    private boolean suspended;
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getLastLoginAt() {
        return lastLoginAt;
    }

    public void setLastLoginAt(String lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Determines whether this User is a match given the field to search
     * and value to search for
     *
     * @param userSearchField
     * @param searchValue
     * @return
     */
    public boolean isSearchFieldMatch(UserField userSearchField, String searchValue) {
        switch (userSearchField) {
            case ID:
                try {
                    return this.getId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for User ID");
                }
                break;
            case URL:
                return StringUtils.equals(this.getUrl(), searchValue);
            case EXTERNAL_ID:
                return StringUtils.equals(this.getExternalId(), searchValue);
            case NAME:
                return StringUtils.equals(this.getName(), searchValue);
            case ALIAS:
                return StringUtils.equals(this.getAlias(), searchValue);
            case CREATED_AT:
                return StringUtils.equals(this.getCreatedAt(), searchValue);
            case ACTIVE:
                return this.isActive() == Boolean.parseBoolean(searchValue);
            case VERIFIED:
                return this.isVerified() == Boolean.parseBoolean(searchValue);
            case SHARED:
                return this.isShared() == Boolean.parseBoolean(searchValue);
            case LOCALE:
                return StringUtils.equals(this.getLocale(), searchValue);
            case TIMEZONE:
                return StringUtils.equals(this.getTimezone(), searchValue);
            case LAST_LOGIN_AT:
                return StringUtils.equals(this.getLastLoginAt(), searchValue);
            case EMAIL:
                return StringUtils.equals(this.getEmail(), searchValue);
            case PHONE:
                return StringUtils.equals(this.getPhone(), searchValue);
            case SIGNATURE:
                return StringUtils.equals(this.getSignature(), searchValue);
            case ORGANIZATION_ID:
                try {
                    return this.getOrganizationId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for Organization ID");
                }
                break;
            case TAGS:
                for (String tag : this.getTags()) {
                    if (StringUtils.equals(tag, searchValue)) {
                        return true;
                    }
                }
                break;
            case SUSPENDED:
                return this.isSuspended() == Boolean.parseBoolean(searchValue);
            case ROLE:
                return StringUtils.equals(this.getRole(), searchValue);
        }
        return false;
    }

    /**
     * Override the default toString to print this User in json.
     *
     * @return
     */
    @Override
    public String toString() {
        return GSON.toJson(this);
    }

    /**
     * This class gives us the 'magic' we need to be able to take a string search term
     * and match it to the desired field we're trying to search for.
     */
    public enum UserField {
        ID("_id"),
        URL("url"),
        EXTERNAL_ID("external_id"),
        NAME("name"),
        ALIAS("alias"),
        CREATED_AT("created_at"),
        ACTIVE("active"),
        VERIFIED("verified"),
        SHARED("shared"),
        LOCALE("locale"),
        TIMEZONE("timezone"),
        LAST_LOGIN_AT("last_login_at"),
        EMAIL("email"),
        PHONE("phone"),
        SIGNATURE("signature"),
        ORGANIZATION_ID("organization_id"),
        TAGS("tags"),
        SUSPENDED("suspended"),
        ROLE("role");

        private final String displayText;

        UserField(String displayText) {
            this.displayText = displayText;
        }

        public static UserField getUserFieldFromDisplayText(String fieldText) {
            for (UserField field : UserField.values()) {
                if (StringUtils.equals(field.getDisplayText(), fieldText)) {
                    return field;
                }
            }
            return null;
        }

        public String getDisplayText() {
            return displayText;
        }

    }
}
