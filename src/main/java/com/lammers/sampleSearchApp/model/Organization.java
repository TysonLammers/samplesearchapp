package com.lammers.sampleSearchApp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Organization {
    private static final Logger LOGGER = LogManager.getLogger(Organization.class);
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    @SerializedName("_id")
    private int id;
    private String url;
    private String externalId;
    private String name;
    private List<String> domainNames;
    private String createdAt; // this is actually a timestamp but requirements state 'match string'
    private String details;
    private boolean sharedTickets;
    private List<String> tags;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDomainNames() {
        return domainNames;
    }

    public void setDomainNames(List<String> domainNames) {
        this.domainNames = domainNames;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isSharedTickets() {
        return sharedTickets;
    }

    public void setSharedTickets(boolean sharedTickets) {
        this.sharedTickets = sharedTickets;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * Determines whether this Organization is a match given the field to search
     * and value to search for
     *
     * @param userSearchField
     * @param searchValue
     * @return
     */
    public boolean isSearchFieldMatch(OrganizationField userSearchField, String searchValue) {
        switch (userSearchField) {
            case ID:
                try {
                    return this.getId() == Integer.parseInt(searchValue);
                } catch (NumberFormatException e) {
                    System.out.println("That wasn't a valid number. Returning to main menu...");
                    LOGGER.error("Incompatible type to compare for Organization ID");
                }
                break;
            case URL:
                return StringUtils.equals(this.getUrl(), searchValue);
            case NAME:
                return StringUtils.equals(this.getName(), searchValue);
            case TAGS:
                for (String tag : this.getTags()) {
                    if (StringUtils.equals(tag, searchValue)) {
                        return true;
                    }
                }
                break;
            case DETAILS:
                return StringUtils.equals(this.getDetails(), searchValue);
            case CREATED_AT:
                return StringUtils.equals(this.getCreatedAt(), searchValue);
            case EXTERNAL_ID:
                return StringUtils.equals(this.getExternalId(), searchValue);
            case DOMAIN_NAMES:
                for (String domain : this.getDomainNames()) {
                    if (StringUtils.equals(domain, searchValue)) {
                        return true;
                    }
                }
                break;
            case SHARED_TICKETS:
                return this.isSharedTickets() == Boolean.parseBoolean(searchValue);
        }
        return false;
    }

    /**
     * Override the default toString to print this organization in Json
     *
     * @return
     */
    @Override
    public String toString() {
        return GSON.toJson(this);
    }

    /**
     * This class gives us the 'magic' we need to be able to take a string search term
     * and match it to the desired field we're trying to search for.
     */
    public enum OrganizationField {
        ID("_id"),
        URL("url"),
        EXTERNAL_ID("external_id"),
        NAME("name"),
        DOMAIN_NAMES("domain_names"),
        CREATED_AT("created_at"),
        DETAILS("details"),
        SHARED_TICKETS("shared_tickets"),
        TAGS("tags");

        private final String displayText;

        OrganizationField(String displayText) {
            this.displayText = displayText;
        }

        public static OrganizationField getFieldFromDisplayText(String displayText) {
            for (OrganizationField field : OrganizationField.values()) {
                if (StringUtils.equals(field.getDisplayText(), displayText)) {
                    return field;
                }
            }
            LOGGER.error("Invalid field name received for Organization class");
            return null;
        }

        public String getDisplayText() {
            return displayText;
        }
    }
}
