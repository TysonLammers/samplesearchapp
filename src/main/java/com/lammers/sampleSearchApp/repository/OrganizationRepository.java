package com.lammers.sampleSearchApp.repository;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lammers.sampleSearchApp.model.Organization;
import com.lammers.sampleSearchApp.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.lammers.sampleSearchApp.model.Organization.OrganizationField;

public class OrganizationRepository {
    private static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting().serializeNulls().create();
    // filename defaults for data used in application - these are all in the
    // resources folder by default based on classpath config from pom
    public static String ORGANIZATIONS_DEFAULT_FILENAME = "organizations.json";
    private static Logger LOGGER = LogManager.getLogger(OrganizationRepository.class);
    private List<Organization> organizations;

    /**
     * No-args constructor loads default data provided in
     * the sample json files.
     */
    public OrganizationRepository() {
        organizations = loadOrganizations(ORGANIZATIONS_DEFAULT_FILENAME);
    }

    /**
     * One-arg constructor loads organizations from the provided json filename
     * on the classpath.
     *
     * @param filename
     */
    public OrganizationRepository(String filename) {
        organizations = loadOrganizations(filename);
    }

    /**
     * Returns a list of possible human-readable fields that can be searched
     *
     * @return
     */
    public static List<String> getListOfSearchableFields() {
        List<String> strings = new ArrayList<>();
        Arrays.asList(OrganizationField.values()).stream()
                .forEach(c -> strings.add(c.getDisplayText()));
        return strings;
    }

    /**
     * Protected getters/setters are mostly for unit tests
     *
     * @return
     */
    protected List<Organization> getOrganizations() {
        return organizations;
    }

    protected void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }

    /**
     * Public method to retrieve the number of organizations currently loaded
     *
     * @return
     */
    public int getSize() {
        return organizations.size();
    }

    public List<Organization> findOrganizationByFieldAndValue(OrganizationField field, String value) {
        List<Organization> matchingOrgs = new ArrayList<>();
        for (Organization org : organizations) {
            if (org.isSearchFieldMatch(field, value)) {
                matchingOrgs.add(org);
            }
        }
        return matchingOrgs;
    }

    /**
     * Loads the file indicated by ORGANIZATIONS_FILENAME from the classpath
     * and uses Gson to parse them into a list of objects we can work with.
     *
     * @return
     */
    protected List<Organization> loadOrganizations(String filename) {
        String orgsJson = FileUtils.readFileContentsFromClasspath(filename);

        if (StringUtils.isBlank(orgsJson)) {
            LOGGER.error("Could not retrieve organizations json content. Using empty array.");
            return new ArrayList<>();
        }
        Organization[] orgs = GSON.fromJson(orgsJson, Organization[].class);
        if (orgs == null || orgs.length == 0) {
            LOGGER.error("Did not parse any organizations from json file " + filename + " - Using empty array");
            return new ArrayList<>();
        }
        LOGGER.info("Loaded " + orgs.length + "organizations from file " + filename);
        return Arrays.asList(orgs);
    }

}
