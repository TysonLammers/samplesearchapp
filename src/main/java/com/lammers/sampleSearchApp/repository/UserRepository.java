package com.lammers.sampleSearchApp.repository;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lammers.sampleSearchApp.model.User;
import com.lammers.sampleSearchApp.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.lammers.sampleSearchApp.model.User.UserField;

public class UserRepository {
    private static final Logger LOGGER = LogManager.getLogger(UserRepository.class);
    private static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting().serializeNulls().create();
    private static String USERS_DEFAULT_FILENAME = "users.json";

    private List<User> users;

    /**
     * No-args constructor loads default data provided in
     * the sample json files.
     */
    public UserRepository() {
        users = loadUsers(USERS_DEFAULT_FILENAME);
    }

    /**
     * One-arg constructor loads organizations from the provided json filename
     * on the classpath.
     *
     * @param filename
     */
    public UserRepository(String filename) {
        users = loadUsers(filename);
    }

    /**
     * Loads the file indicated by USERS_FILENAME from the classpath
     * and uses Gson to parse them into a list of objects we can work with.
     *
     * @return
     */
    protected static List<User> loadUsers(String filename) {
        String userJson = FileUtils.readFileContentsFromClasspath(filename);

        if (StringUtils.isBlank(userJson)) {
            LOGGER.error("Could not retrieve users json content. Using empty array.");
            return new ArrayList<>();
        }
        User[] users = GSON.fromJson(userJson, User[].class);
        if (users == null || users.length == 0) {
            LOGGER.error("Did not parse any users - Using empty array");
            return new ArrayList<>();
        }
        return Arrays.asList(users);
    }

    /**
     * Returns a list of human-readable fields that can be searched
     *
     * @return
     */
    public static List<String> getListOfSearchableFields() {
        List<String> strings = new ArrayList<>();
        Arrays.asList(UserField.values()).stream()
                .forEach(c -> strings.add(c.getDisplayText()));
        return strings;
    }

    public List<User> findUsersByFieldAndValue(UserField field, String value) {
        List<User> matchingUsers = new ArrayList<>();
        for (User user : users) {
            if (user.isSearchFieldMatch(field, value)) {
                matchingUsers.add(user);
            }
        }
        return matchingUsers;
    }

    /**
     * public method to retrieve the number of users loaded
     *
     * @return
     */
    public int getSize() {
        return users.size();
    }

    /**
     * protected getter/setters are mostly for unit tests
     *
     * @return
     */
    protected List<User> getUsers() {
        return users;
    }

    protected void setUsers(List<User> users) {
        this.users = users;
    }

}
