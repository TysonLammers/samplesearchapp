package com.lammers.sampleSearchApp.repository;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lammers.sampleSearchApp.model.Ticket;
import com.lammers.sampleSearchApp.utils.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.lammers.sampleSearchApp.model.Ticket.TicketField;

public class TicketRepository {
    private static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting().serializeNulls().create();
    private static Logger LOGGER = LogManager.getLogger(TicketRepository.class);
    // filename defaults for data used in application - these are all in the
    // resources folder by default based on classpath config from pom
    private static String TICKETS_DEFAULT_FILENAME = "tickets.json";

    private List<Ticket> tickets;

    /**
     * No-args constructor loads default data provided
     * in the sample json files
     */
    public TicketRepository() {
        tickets = loadTickets(TICKETS_DEFAULT_FILENAME);
    }

    /**
     * One-arg constructor loads tickets from the provided json
     * filename on the classpath
     *
     * @param filename
     */
    public TicketRepository(String filename) {
        tickets = loadTickets(filename);
    }

    /**
     * Loads the file with the name passed in from the classpath
     * and uses Gson to parse them into a list of objects we can work with.
     *
     * @return
     */
    protected static List<Ticket> loadTickets(String filename) {
        String ticketJson = FileUtils.readFileContentsFromClasspath(filename);

        if (StringUtils.isBlank(ticketJson)) {
            LOGGER.error("Could not retrieve tickets json content. Using empty array");
            return new ArrayList<>();
        }
        Ticket[] tickets = GSON.fromJson(ticketJson, Ticket[].class);
        if (tickets == null || tickets.length == 0) {
            LOGGER.error("Did not parse any tickets - Using empty array");
            return new ArrayList<>();
        }
        return Arrays.asList(tickets);
    }

    /**
     * Returns a list of human-readable fields that can be searched
     *
     * @return
     */
    public static List<String> getListOfSearchableFields() {
        List<String> strings = new ArrayList<>();
        Arrays.asList(TicketField.values()).stream()
                .forEach(c -> strings.add(c.getDisplayText()));
        return strings;
    }

    public List<Ticket> findTickesByFieldAndValue(TicketField field, String value) {
        List<Ticket> matchingTickets = new ArrayList<>();
        for (Ticket ticket : tickets) {
            if (ticket.isSearchFieldMatch(field, value)) {
                matchingTickets.add(ticket);
            }
        }
        return matchingTickets;
    }

    /**
     * public method to get the number of tickets loaded
     *
     * @return
     */
    public int getSize() {
        return tickets.size();
    }

    /**
     * Protected setters/getters are mostly for unit tests
     *
     * @return
     */
    protected List<Ticket> getTickets() {
        return tickets;
    }

    protected void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
