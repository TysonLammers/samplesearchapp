package com.lammers.sampleSearchApp;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lammers.sampleSearchApp.model.Organization;
import com.lammers.sampleSearchApp.model.Ticket;
import com.lammers.sampleSearchApp.model.User;
import com.lammers.sampleSearchApp.repository.OrganizationRepository;
import com.lammers.sampleSearchApp.repository.TicketRepository;
import com.lammers.sampleSearchApp.repository.UserRepository;
import com.lammers.sampleSearchApp.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;

import static com.lammers.sampleSearchApp.model.Organization.OrganizationField;
import static com.lammers.sampleSearchApp.model.Ticket.TicketField;
import static com.lammers.sampleSearchApp.model.User.UserField;

public class SampleSearchApp {
    public static final Gson GSON = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setPrettyPrinting().serializeNulls().create();
    private static final Logger LOGGER = LogManager.getLogger(SampleSearchApp.class);

    private static final String LINE_SEPARATOR = "-----------------------\n";
    private static final String EXIT_CONDITION_TEXT = "quit";
    private final InputStream inputStream;
    private final PrintStream outputStream;
    // repositories hold the data and house individual search functions
    protected OrganizationRepository organizationRepository;
    protected UserRepository userRepository;
    protected TicketRepository ticketRepository;
    private boolean shouldExit = false;

    /**
     * No-args constructor loads the default data provided in json files provided
     * as examples from the classpath.
     */
    protected SampleSearchApp() {
        inputStream = System.in;
        outputStream = System.out;
        organizationRepository = new OrganizationRepository();
        userRepository = new UserRepository();
        ticketRepository = new TicketRepository();
    }

    /**
     * This constructor overrides the default input stream (System.in)
     * to allow us to pass input to the application via any text resource
     * (console or file)
     */
    protected SampleSearchApp(InputStream in, PrintStream out) {
        inputStream = in;
        outputStream = out;
        organizationRepository = new OrganizationRepository();
        userRepository = new UserRepository();
        ticketRepository = new TicketRepository();
    }

    /**
     * Main entry method (the "runner") for the application
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        SampleSearchApp app = new SampleSearchApp();
        app.getOutputStream().println("SampleSearchApp\n************");


        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Orgs size: " + app.getOrganizationRepository().getSize() + ", tickets size: " /*+ tickets.size() + ", users size: " + users.size()*/);
        }

        app.runMainLoop();

        app.getOutputStream().println("Exiting application...");
    }

    private static void printSearchableFields(PrintStream outputStream) {
        StringBuilder sb = new StringBuilder();
        sb.append(LINE_SEPARATOR);
        sb.append("Organization search fields: \n");
        for (String field : OrganizationRepository.getListOfSearchableFields()) {
            sb.append("* ");
            sb.append(field);
            sb.append("\n");
        }
        sb.append(LINE_SEPARATOR);
        sb.append("Ticket Search Fields: \n");
        for (String field : TicketRepository.getListOfSearchableFields()) {
            sb.append("* ");
            sb.append(field);
            sb.append("\n");
        }
        sb.append(LINE_SEPARATOR);
        sb.append("User Search Fields: \n");
        for (String field : UserRepository.getListOfSearchableFields()) {
            sb.append("* ");
            sb.append(field);
            sb.append("\n");
        }
        outputStream.println(sb.toString());
    }

    /**
     * Entry point for when the user selects "search" from the 'main menu'
     */
    private void enterSearchLoop() {
        outputStream.println("Which type of object would you like to search?");
        outputStream.println("Enter 1 for Users, 2 for Tickets, or 3 to search Organizations");
        int inputNum = -1;

        while (inputNum == -1) {
            String userInput = IOUtils.getStringFromInputStream(inputStream);
            if (checkForExitCondition(userInput)) {
                return;
            }
            try {
                inputNum = Integer.parseInt(userInput);
                switch (inputNum) {
                    case 1:
                        enterSearchUserLoop();
                        break;
                    case 2:
                        enterSearchTicketLoop();
                        break;
                    case 3:
                        enterSearchOrganizationLoop();
                        break;
                    default:
                        printErrorText();
                        break;
                }
            } catch (NumberFormatException e) {
                LOGGER.error("Could not parse user input for search target object");
            }
        }
    }

    /**
     * Parse and determine which field within user we are searching on,
     * then find and print all matches that we find, or a message indicating
     * no matches were found.
     */
    private void enterSearchUserLoop() {
        outputStream.println("Enter field to search Users for: ");
        // first get the user field that is being searched through
        UserField searchField = null;
        while (searchField == null) {
            String userFieldInput = IOUtils.getStringFromInputStream(inputStream);
            if (checkForExitCondition(userFieldInput)) {
                return;
            }
            searchField = UserField.getUserFieldFromDisplayText(userFieldInput);
            if (searchField == null) {
                outputStream.println("Invalid entry. Try again.");
            }
        }
        // after we have the field, get the value to search for
        outputStream.println("Enter value to search for: ");
        String userValueInput = IOUtils.getStringFromInputStream(inputStream);
        if (checkForExitCondition(userValueInput)) {
            return;
        }
        // once we have the field and value, do the search
        List<User> matchingUsers = userRepository.findUsersByFieldAndValue(searchField, userValueInput);
        if (matchingUsers.isEmpty()) {
            outputStream.println("No results found.");
        } else {
            for (User user : matchingUsers) {
                outputStream.println(user.toString());
            }
        }
    }

    /**
     * Parse and determine which field within ticket we are searching on,
     * then find and print all matches that we find, or a message indicating
     * no matches were found.
     */
    private void enterSearchTicketLoop() {
        outputStream.println("Enter field to search Users for: ");
        // first get the field the user is searching through
        TicketField searchField = null;
        while (searchField == null) {
            String userFieldInput = IOUtils.getStringFromInputStream(inputStream);
            if (checkForExitCondition(userFieldInput)) {
                return;
            }
            searchField = TicketField.getTicketFieldFromDisplayText(userFieldInput);
            if (searchField == null) {
                outputStream.println("Invalid entry. Try again.");
            }
        }
        // once we have the field, get the value the user is searching for
        outputStream.println("Enter value to search for: ");
        String userValueInput = IOUtils.getStringFromInputStream(inputStream);
        if (checkForExitCondition(userValueInput)) {
            return;
        }

        // once we've got the field and value, do the search
        List<Ticket> matchingTickets = ticketRepository.findTickesByFieldAndValue(searchField, userValueInput);
        if (matchingTickets.isEmpty()) {
            outputStream.println("No results found.");
        } else {
            for (Ticket ticket : matchingTickets) {
                outputStream.println(ticket.toString());
            }
        }
    }

    /**
     * Parse and determine which field within organization we are searching on,
     * then find and print all matches that we find, or a message indicating
     * no matches were found.
     */
    private void enterSearchOrganizationLoop() {
        outputStream.println("Enter field to search Organizations for: ");
        // first get the organization field that is being searched through
        OrganizationField searchField = null;
        while (searchField == null) {
            String userFieldInput = IOUtils.getStringFromInputStream(inputStream);
            if (checkForExitCondition(userFieldInput)) {
                return;
            }
            searchField = OrganizationField.getFieldFromDisplayText(userFieldInput);
            if (searchField == null) {
                outputStream.println("Invalid entry. Try again.");
            }
        }
        // after we have the field, we get the value to search for
        outputStream.println("Enter value to search for: ");
        String userValueInput = IOUtils.getStringFromInputStream(inputStream);
        if (checkForExitCondition(userValueInput)) {
            return;
        }
        // once we've got the field and value, do the search
        List<Organization> matchingOrgs = organizationRepository.findOrganizationByFieldAndValue(searchField, userValueInput);
        if (matchingOrgs.isEmpty()) {
            outputStream.println("No results found.");
        } else {
            for (Organization org : matchingOrgs) {
                outputStream.println(org.toString());
            }
        }
    }

    private boolean checkForExitCondition(String input) {
        if (!StringUtils.isEmpty(input) && StringUtils.equals(EXIT_CONDITION_TEXT, input)) {
            outputStream.println("Exit received. Exiting application..");
            shouldExit = true;
            return true;
        }
        return false;
    }

    private void printErrorText() {
        outputStream.println("Did not recognize input. Please try again.\n" + LINE_SEPARATOR);
    }

    /**
     * This method holds the core logic loop that reads the user's input and performs the corresponding actions.
     */
    public void runMainLoop() {
        while (!shouldExit) {
            outputStream.println("Welcome to the sample search app.\n" +
                    "Type \"quit\" to quit at any time; press \"Enter\" to submit\n" +
                    "What would you like to do?\n" +
                    "* Enter 1 to search\n" +
                    "* Enter 2 to view list of searchable fields\n");
            // grab user's input
            String userInput = IOUtils.getStringFromInputStream(inputStream);
            if (checkForExitCondition(userInput)) {
                outputStream.println("Exiting application...");
                break;
            }
            int inputNumber = -1;
            try {
                inputNumber = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                LOGGER.error("Could not parse user's input into a number at main menu");
            }

            // branch based on what user wants to do
            switch (inputNumber) {
                case 1:
                    enterSearchLoop();
                    break;
                case 2:
                    printSearchableFields(outputStream);
                    break;
                default:
                    printErrorText();
                    break;
            }
        }
    }

    /**
     * Protected setters/getters are mostly for unit tests
     *
     * @return
     */
    OrganizationRepository getOrganizationRepository() {
        return organizationRepository;
    }

    protected void setOrganizationRepository(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    UserRepository getUserRepository() {
        return userRepository;
    }

    protected void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    TicketRepository getTicketRepository() {
        return ticketRepository;
    }

    protected void setTicketRepository(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    protected InputStream getInputStream() {
        return inputStream;
    }

    protected PrintStream getOutputStream() {
        return outputStream;
    }

    protected boolean isShouldExit() {
        return shouldExit;
    }

    protected void setShouldExit(boolean shouldExit) {
        this.shouldExit = shouldExit;
    }
}
