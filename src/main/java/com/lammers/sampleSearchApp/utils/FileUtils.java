package com.lammers.sampleSearchApp.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileUtils {
    private static final Logger LOGGER = LogManager.getLogger(FileUtils.class);

    /**
     * Reads a  file from the classpath and returns the contents.
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public static String readFileContentsFromClasspath(String fileName) {
        StringBuilder resultStringBuilder = new StringBuilder();
        InputStream input = FileUtils.class.getClassLoader().getResourceAsStream(fileName);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(input))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            LOGGER.error("Failed to parse file with name: " + fileName + " - caught IOException. Is the file on the classpath?");
        }
        return resultStringBuilder.toString();
    }
}
