package com.lammers.sampleSearchApp.utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.time.LocalDateTime;

/**
 * A class to hold some of the custom things that I had to do
 * in order to make Gson play nicely with the sample data.
 */
public class GsonUtils {
    private static final Logger LOGGER = LogManager.getLogger(GsonUtils.class);

    /**
     * Public-exposed method that creates a new serializer.
     *
     * @return
     */
    public static LocalDateTimeDeserializer getDateDeserializer() {
        return new LocalDateTimeDeserializer();
    }

    /**
     * A custom-written deserializer that can parse a String into a LocalDateTime.
     * Private so that implementation of deserializer is hidden from calling methods.
     * LocalDateTimes are so much easier to use, it's worth it.
     */
    private static class LocalDateTimeDeserializer implements JsonDeserializer<LocalDateTime> {
        public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            //TODO: implement parser for the timestamp
            return LocalDateTime.now();
        }
    }
}
