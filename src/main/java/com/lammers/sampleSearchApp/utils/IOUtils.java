package com.lammers.sampleSearchApp.utils;

import java.io.InputStream;
import java.util.Scanner;

public class IOUtils {
    /**
     * Gets an integer from the inputstream. Uses negative numbers as sentinel
     * values (i.e. if a negative number is returned, it was invalid/did not
     * capture input).
     * <p>
     * This is abstracted to take an InputStream to make the program more testable.
     *
     * @param in
     * @return
     */
    public static String getStringFromInputStream(InputStream in) {
        Scanner scanner = new Scanner(in);
        return scanner.nextLine();
    }


}
